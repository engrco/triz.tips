---
title: CRNC.work
subtitle: Distributed currency or NFT application for credentialing
comments: false
---

Let's think about a Workflow-As-Currency DAPP ... CRNC.WORK ... at first, let's just think about what a distributed economic exchanged app should like as an IDEAL state.

# What does the IDEAL final state look like

The workflow-as-currency DAPP is about credentialling. The CNRC.WORK approach would be based upon using actual real live work AND formalize the process of listening and RESPONDING TO comments/critiques/suggestions/observations that "customers" of the work make.  

The CRNC.WORK process of credentially, certifying or proving skills with a portfolio would follow the methodolgy of the [Gradiance approach to pedagogy](https://www.gradiance.com/idea.html) ... the IMPORTANT part of doing the work is not exactly about getting it PERFECT on the first go, but what is really more achievable, practical and beneficial in the long run is about LISTENING and RESPONDING ... to rapidly converge on the *customer's* view of perfection RATHER than the student's view of perfection.

Of course, much of this is already in place, sans Gradiance approach, in merge requests, commitgraphs, history of published articles, resume, CV, even checkstub history.

Security and authorization will rather OBVIOUSLY always be issues; there will be NO END to the infosecOps on any currency that matters.  Different information would need to be visible or secure at different levels -- and yet available under the appropriate validations for someone doing a background/reference check AND the validation/verification would ALSO leave a traceable commitgraph, ie like quantum crypto, anyone who even had read access would leave a trace.  The DAPP would be more than just a dead, lifeless record ... it would also be an intelligent bot, searching for gigs or talent needs ... beyond that it would also serve as wallet or a conduit for getting paid ... so without being like a swiss army knife tool-for-every-scenario, it would be the basis of a a professional talent account DAPP ecosystem ... of course, this talent/finance ecosystem seems like it could be become such a basic and elegant, yet absolutely essential idea ... something nobody could live without ... basically like PayPal was 20-25 years ago, excpet 10X better and based on the wealth that every human has, ie time/bankable skills/proven achievements ... 

# HOPEFULLY ... somebody has already done this ... or something similar in microfinance

Perhaps it might be something like a microfinance operating system ... remember the Grameen Microfinance miracle of twenty years ago [and still exists] ... remember Kiva and peer-to-peer lending ... well, this is not exactly about microcredit -- it's about microtalent, EXCEPT that's a problem, because everybody wants to think that they are Big Talent ... except that practically speaking, this IS really about the micro-ness of talent ... about getting one's reputation out there, being known for one's skills, earning top dollar for top skills ... but it's VERY IMPORTANT ... that this is really about overcoming the problems of micro-ness of talent WITHOUT the inevitable problem of 10 billion points of BRIGHT LIGHT, each trying to be the ONLY light.