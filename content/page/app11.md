---
title: Inventive Problem Solving
subtitle: TRIZ or ARIZ or ???
comments: false
---

Innovation is 99% perspiration, 1% inspiration ... this app is about automating the tedious 99%, eg looking through the last 20 years of patents for similarities / differences to the problem at hand.