---
title: Accelerated Negotiation App
subtitle: Auction tool, could be used to negotiate OR set up bidding between parties
comments: false
---

It's an app for selling anything, optimized for time [as a work slave].  Auctions only really work if you have at least two interested parties ... more is better ... in a typical job negotiation, the employer is one bidder and your own creative pursuits are another bidder -- except that at some point, your creative pursuits might not be able to bid very much for your time.