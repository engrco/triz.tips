---
title: Git Repository Curator
subtitle: To find / establish connections with Devs doing interesting work 
comments: false
---


Something along the lines of [ConnectedPapers](https://www.connectedpapers.com/about) ... using semantic AI/ML to build different dimensions of connections to Git repositories from Gitlab, GitHub, other sources