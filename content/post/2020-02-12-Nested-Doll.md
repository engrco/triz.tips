---
title: Nested Doll
subtitle: Place one object inside another
date: 2020-02-12
bigimg: [{src: "/img/path.jpg", desc: "Path"}]
---

Nested Doll

* Russian doll inside a doll inside a doll inside a doll
* Measuring cups or spoons
* Portable audio system (microphone fits inside transmitter, which fits inside amplifier case)
* Make one part pass through a cavity in the other.
* Telescoping radio antenna, pointer, telephoto lens
* Seat belt retraction mechanism
* Retractable aircraft landing gear stow inside the fuselage (also demonstrates Principle 15, Dynamism).