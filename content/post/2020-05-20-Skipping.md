---
title: Skipping, microenvironments, cellular organelles
subtitle: Perform a process to the skip harmful damage 
date: 2020-05-20
tags: ["Skipping", "Principles"]
---


Skipping, microenvironments, cellular organelles

* Conduct a process , or certain stages (e.g. destructible, harmful or hazardous operations) at high speed.
* Use a high speed dentist’s drill to avoid heating tissue.
* Cut plastic faster than heat can propagate in the material, to avoid deforming the shape.
