---
title: Blessing In Disguise
subtitle: Use harmful factors to achieve a positive effect.
date: 2022-05-10
tags: ["Blessing", "Principles"]
---


# Blessing in disguise

**“Turn Lemons into Lemonade”**

* Poison, venom, biochemicals to subdue an enemy OR as drug to trigger a response. Use waste heat to generate electric power.
* Recycle waste (scrap) material from one process as raw materials for another.
* Eliminate the primary harmful action by adding it to another harmful action to resolve the problem.
* Add a buffering material to a corrosive solution. Use a helium-oxygen mix for diving, to eliminate both nitrogen narcosis and oxygen poisoning from air and other nitrox mixes.
* Amplify a harmful factor to such a degree that it is no longer harmful.*
Use a backfire to eliminate the fuel from a forest fire.
