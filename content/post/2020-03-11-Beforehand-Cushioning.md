---
title: Beforehand Cushioning
subtitle: Set Up A Graceful Failure
date: 2020-03-11
tags: ["Beforehand Cushioning", "Principles"]
---

Beforehand Cushioning

* Prepare emergency means beforehand to compensate for the relatively low reliability of an object.
* Magnetic strip on photographic film that directs the developer to compensate for poor exposure
* Back-up parachute
* Alternate air system for aircraft instruments
 