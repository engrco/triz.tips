---
title: Anti-Weight
subtitle: Merge weighty components with other objects that provide lift
date: 2020-02-19
tags: ["Anti-Weight", "Principles"]
---

Anti-weight

* Inject foaming agent into a bundle of logs, to make it float better.
* Use helium balloon to support advertising signs.
* To compensate for the weight of an object, make it interact with the environment (e.g. use aerodynamic, hydrodynamic, buoyancy and other forces).
* Aircraft wing shape reduces air density above the wing, increases density below wing, to create lift. (This also demonstrates Principle 4, Asymmetry.)
* Vortex strips improve lift of aircraft wings.
* Hydrofoils lift ship out of the water to reduce drag.