---
title: The Full Ishikawa
subtitle: Five whys, full root cause on every why including "Who benefits by this problem not being solved."
date: 2020-02-19
tags: ["Root Cause", "Foundation"]
comments: false
---

BEFORE we start throwing mud at the wall to see what sticks -- we really need to make a solid effort at understanding the root cause of the problem ... especially when it seems apparent that everything is too convoluted and we cannot hope to understand and we are tempted to just scrap everything and start with something new.

The FOUNDATION of TRIZ.tips is that inventive problem-solving begins with understanding more about the root causes of problems than anyone else bothers to care about.

The PROBLEM is the opportunity ... knocking on your door when you are too tired, too afraid, too ashamed, too confused to go to the door and answer it.

# The PROBLEM is the opportunity.

Because, genuine problem-solving begins with an unquestioning, unconditional LOVE the problem ... we ask at least five levels of why ... but probably a lot more.

At each level why, we really need to look DEEPLY and BROADLY at that level.  Experience has taught us that at each level, there are at least six to eight different general facets of the meatter ... your exact characterization or semantics for the kind that you ordinarily encounter will vary ... but you must attempt to be aware of the entire situation impacting the matter ... at a miniumum, you must look for:

* Maintenance (discipline, habits, LEAN no-wait, no-extra-effort readiness of all systems that might come into play) 
* Method (modifiable but version-controlled and CI/CD steps, software algorithms, ) 
* Machine (inflexible hardware, equipment, facilities, utilities) 
* Material (all inputs and/or data) 
* Man(skills and training) 
* Mgmt(not just supervisors but owners, politicians and powerful stakeholders) 
* Miscellaneous (catch-all parking lot of anomalies, oddities, x-factors)
* Motherfuckers (malicious beneficiaries who are way too attractive, eg $5000/night hookers, to be there offering solutions like drug pushers selling Std-of-Care insurance-approved prescription pharma)