---
title: Vibration or oscillation
subtitle: Always Be Flanking -- when going Through is out, try Up, Around, Over, Under, RF, Virtual,
date: 2020-04-22
tags: ["Vibration", "Principles"]
---

Vibration or oscillation

* Cause an object to oscillate or vibrate.
* Electric carving knife with vibrating blades
* Increase/decrease amplitude or frequency.
* Distribute powder with vibration.
* Objects/structures will vibrate at resonant frequency.
* Spectral peaks reveal much about the nature of object/structure/problem.
* Use piezoelectric vibrators, ie speakers, instead of mechanical ones for digital control.
* Quartz crystal oscillations drive high accuracy clocks.
* Use combined ultrasonic and electromagnetic field oscillations.
* Mixing/stirring alloys in an induction furnace
 