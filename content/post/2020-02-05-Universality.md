---
title: Universality
subtitle: Make a part or object perform multiple functions
date: 2020-02-20
tags: ["Universality", "Principles"]
---

Universality

* Standardize to eliminate the need for other parts, other tools, other skills.
* Handle of a toothbrush contains toothpaste
* Child’s car safety seat converts to a stroller
* Mulching lawnmower (*An example of self-referentiality as some examples are demonstrate two Principles, in this case, 5 and 6, Merging and Universality.*)
* Team leader acts as recorder and timekeeper.
* CCD (Charge coupled device) with micro-lenses formed on the surface