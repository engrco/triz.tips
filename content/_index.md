In an extremely general sense, TRIZ.tips is about [knowledge engineering or the development of accelerated decision support systems](https://en.wikipedia.org/wiki/Knowledge_engine). The decision support system uses intelligence gathered in a [technological intelligence gathering](https://en.wikipedia.org/wiki/Technical_intelligence) process. 

More specifically, TRIZ.tips is about AI/ML learning algorithms which are loosely based upon the TRIZ algorithmic approach ...теория решения изобретательских задач...Theory of the Resolution of Invention-related Z...TIPS...Theory of Inventive Problem Solving. The algorithm of inventive problem-solving (ARIZ) was originally derived thru the brute force analysis of patents along with the most sophisticated and intense, but highly informed and specific intelligence gathering of its day. Finding an answer will always involve practical hands-on work, but if you want to find a needle in a haystack somewhere, you should at least narrow down the list of potential haystacks in the universe of where to focus your search.

Knowledge engines are no different than search engines -- the speed of answers comes from all of the information that has been gathered, sorted, indexed and mapped before the request comes in.

The TRIZ.tips knowledge engine project is about the practical application of AI/ML to the problem of innovation and inventive problem-solving ... it's really about analyzing things like patents or software repositories as data points; then curating data sets of data points and finally applying analysis to lists of curated data sets ... at its core, TRIZ is massively experiental, depending upon the collective experiences of millions of inventors who have been inventing for centuries. Thus TRIZ.tips involves the application of AI/ML to recognize patterns or simularities in problem-solving scenarios.

Of course the foundation for any problem solving effort will start with serious root-cause analysis and deep understanding of the problem, BEFORE ever beginning to model solutions in the virtual realm or to propose fixes in the physical realm, ie the more complete the understanding of the problem, the FASTER and more elegant the set of proposed ideas from the knowledge engine will be ... shooting from the hip results in nothing more than bandaids that will not actually do much to solve the problem in a permanent manner.

The antecedent of thinking behind the the development of TRIZ knowledge engine algorithms would be the [set of 40 principles of innovation from the TRIZ algorithm of inventive problem solving](https://triz-journal.com/40-inventive-principles-examples/) ... at this point, we are using TRIZ as a new musician would use a venerable jazz standard as reference point for practicing ... we are riffing on the standard, adding new instruments, new players. 

If you want to investigate or clone what we're doing with TRIZ.tips, just head over to the [GitLab project](https://gitlab.com/MarkBruns/triz.tips), but right now, it's clearly a side project that is, occasionally tinkered with, but very much still UNDER CONSTRUCTION.

We will start this whole exercise by practicing, riffing, blogging, experimenting and then improvizing within this framework of 40 principles of innovation.

1) Segmentation

2) Separation

3) Local Quality

4) Asymmetry

5) Merging

6) Universality

7) Nested Doll

8) Anti-weight

9) Preliminary anti-action

10) Preliminary action

11) Beforehand cushioning

12) Equipotentiality

13) ‘The other way round’ or Chirality 

14) Spheroidality – Curvature

15) Dynamics

16) Partial or Excessive Actions

17) Another Dimension

18) Vibration or oscillation

19) Periodic or pulsed action

20) Continuity of useful action

21) Skipping 

## 22) “Blessing in disguise” or “Turn Lemons into Lemonade”

Use harmful factors (particularly, harmful effects of the environment or surroundings) to achieve a positive effect.
Poison, venom, biochemicals to subdue an enemy OR as drug to trigger a response.
Use waste heat to generate electric power.
Recycle waste (scrap) material from one process as raw materials for another.
Eliminate the primary harmful action by adding it to another harmful action to resolve the problem.
Add a buffering material to a corrosive solution.
Use a helium-oxygen mix for diving, to eliminate both nitrogen narcosis and oxygen poisoning from air and other nitrox mixes.
Amplify a harmful factor to such a degree that it is no longer harmful.
Use a backfire to eliminate the fuel from a forest fire.
 

## 23) Feedback, signalling, [amino acids](https://en.wikipedia.org/wiki/Amino_acid), [adenosine triphosphate](https://en.wikipedia.org/wiki/Adenosine_triphosphate)

Intercellular energy transfer, hormonal signalling, neurotransmission, passing genetic traits
Introduce feedback (referring back, cross-checking) to improve a process or action.
Automatic volume control in audio circuits
Signal from gyrocompass is used to control simple aircraft autopilots.
Statistical Process Control (SPC) — Measurements are used to decide when to modify a process. (Not all feedback systems are automated!)
Budgets –Measurements are used to decide when to modify a process.
If feedback is already used, change its magnitude or influence.
Change sensitivity of an autopilot when within 5 miles of an airport.
Change sensitivity of a thermostat when cooling vs. heating, since it uses energy less efficiently when cooling.
Change a management measure from budget variance to customer satisfaction.
 

## 24) Intermediary, secession species, ecoystem handoffs

Use an intermediary carrier article or intermediary process.
Carpenter’s nailset, used between the hammer and the nail
Merge one object temporarily with another (which can be easily removed).
Pot holder to carry hot dishes to the table
 

Principle 25. Self-service

Make an object serve itself by performing auxiliary helpful functions
A soda fountain pump that runs on the pressure of the carbon dioxide that is used to “fizz” the drinks. This assures that drinks will not be flat, and eliminates the need for sensors.
Halogen lamps regenerate the filament during use–evaporated material is redeposited.
To weld steel to aluminum, create an interface from alternating thin strips of the 2 materials. Cold weld the surface into a single unit with steel on one face and copper on the other, then use normal welding techniques to attach the steel object to the interface, and the interface to the aluminum. (This concept also has elements of Principle 24, Intermediary, and Principle 4, Asymmetry.)
Use waste resources, energy, or substances.
Use heat from a process to generate electricity: “Co-generation”.
Use animal waste as fertilizer.
Use food and lawn waste to create compost.
 

## 26) Copying, DNA

Instead of an unavailable, expensive, fragile object, use simpler and inexpensive copies.
Virtual reality via computer instead of an expensive vacation
Listen to an audio tape instead of attending a seminar.
Replace an object, or process with optical copies.
Do surveying from space photographs instead of on the ground.
Measure an object by measuring the photograph.
Make sonograms to evaluate the health of a fetus, instead of risking damage by direct testing.
If visible optical copies are already used, move to infrared or ultraviolet copies.
Make images in infrared to detect heat sources, such as diseases in crops, or intruders in a security system.
 

Principle 27. Cheap short-living objects

Replace an inexpensive object with a multiple of inexpensive objects, comprising certain qualities (such as service life, for instance).
Use disposable paper objects to avoid the cost of cleaning and storing durable objects. Plastic cups in motels, disposable diapers, many kinds of medical supplies.
 

Principle 28. Mechanics substitution

Replace a mechanical means with a sensory (optical, acoustic, taste or smell) means.
Replace a physical fence to confine a dog or cat with an acoustic “fence” (signal audible to the animal).
Use a bad smelling compound in natural gas to alert users to leakage, instead of a mechanical or electrical sensor.
Use electric, magnetic and electromagnetic fields to interact with the object.
To mix 2 powders, electrostatically charge one positive and the other negative. Either use fields to direct them, or mix them mechanically and let their acquired fields cause the grains of powder to pair up.
Change from static to movable fields, from unstructured fields to those having structure.
Early communications used omnidirectional broadcasting. We now use antennas with very detailed structure of the pattern of radiation.
Use fields in conjunction with field-activated (e.g. ferromagnetic) particles.
Heat a substance containing ferromagnetic material by using varying magnetic field. When the temperature exceeds the Curie point, the material becomes paramagnetic, and no longer absorbs heat.
 

## 29 Viscoeleastics, hydraulics, pneumatics

Use gelled, liquid, gaseous, plasma instead of just solid parts (e.g. inflatable, filled with liquids, air cushion, hydrostatic, hydro-reactive).
Comfortable shoe sole inserts filled with gel
Store energy from decelerating a vehicle in a hydraulic system, then use the stored energy to accelerate later.
 

Principle 30. Flexible shells and thin films

Use flexible shells and thin films instead of three dimensional structures
Use inflatable (thin film) structures as winter covers on tennis courts.
Isolate the object from the external environment using flexible shells and thin films.
Float a film of bipolar material (one end hydrophilic, one end hydrophobic) on a reservoir to limit evaporation.
 

Principle 31. Porous materials

Make an object porous or add porous elements (inserts, coatings, etc.).
Drill holes in a structure to reduce the weight.
If an object is already porous, use the pores to introduce a useful substance or function.
Use a porous metal mesh to wick excess solder away from a joint.
Store hydrogen in the pores of a palladium sponge. (Fuel “tank” for the hydrogen car–much safer than storing hydrogen gas)
 

Principle 32. Color changes

Change the color of an object or its external environment.
Use safe lights in a photographic darkroom.
Change the transparency of an object or its external environment.
Use photolithography to change transparent material to a solid mask for semiconductor processing. Similarly, change mask material from transparent to opaque for silk screen processing.
 

Principle 33. Homogeneity

Make objects interacting with a given object of the same material (or material with identical properties).
Make the container out of the same material as the contents, to reduce chemical reactions.
Make a diamond cutting tool out of diamonds.
 

Principle 34. Discarding and recovering

Make portions of an object that have fulfilled their functions go away (discard by dissolving, evaporating, etc.) or modify these directly during operation.
Use a dissolving capsule for medicine.
Sprinkle water on cornstarch-based packaging and watch it reduce its volume by more than 1000X!
Ice structures: use water ice or carbon dioxide (dry ice) to make a template for a rammed earth structure, such as a temporary dam. Fill with earth, then, let the ice melt or sublime to leave the final structure.
Conversely, restore consumable parts of an object directly in operation.
Self-sharpening lawn mower blades
Automobile engines that give themselves a “tune up” while running (the ones that say “100,000 miles between tune ups”)
 

Principle 35. Parameter changes

A. Change an object’s physical state (e.g. to a gas, liquid, or solid.
Freeze the liquid centers of filled candies, then dip in melted chocolate, instead of handling the messy, gooey, hot liquid.
Transport oxygen or nitrogen or petroleum gas as a liquid, instead of a gas, to reduce volume.
Change the concentration or consistency.
Liquid hand soap is concentrated and more viscous than bar soap at the point of use, making it easier to dispense in the correct amount and more sanitary when shared by several people.
Change the degree of flexibility.
Use adjustable dampers to reduce the noise of parts falling into a container by restricting the motion of the walls of the container.
Vulcanize rubber to change its flexibility and durability.
Change the temperature.
Raise the temperature above the Curie point to change a ferromagnetic substance to a paramagnetic substance.
Raise the temperature of food to cook it. (Changes taste, aroma, texture, chemical properties, etc.)
Lower the temperature of medical specimens to preserve them for later analysis.
 

Principle 36. Phase transitions

Use phenomena occurring during phase transitions (e.g. volume changes, loss or absorption of heat, etc.).
Water expands when frozen, unlike most other liquids. Hannibal is reputed to have used this when marching on Rome a few thousand years ago. Large rocks blocked passages in the Alps. He poured water on them at night. The overnight cold froze the water, and the expansion split the rocks into small pieces which could be pushed aside.
Heat pumps use the heat of vaporization and heat of condensation of a closed thermodynamic cycle to do useful work.
 

Principle 37. Thermal expansion

Use thermal expansion (or contraction) of materials.
Fit a tight joint together by cooling the inner part to contract, heating the outer part to expand, putting the joint together, and returning to equilibrium.
If thermal expansion is being used, use multiple materials with different coefficients of thermal expansion.
The basic leaf spring thermostat: (2 metals with different coefficients of expansion are linked so that it bends one way when warmer than nominal and the opposite way when cooler.)
 

Principle 38. Strong oxidants

Replace common air with oxygen-enriched air.
Scuba diving with Nitrox or other non-air mixtures for extended endurance
Replace enriched air with pure oxygen.
Cut at a higher temperature using an oxy-acetylene torch.
Treat wounds in a high pressure oxygen environment to kill anaerobic bacteria and aid healing.
Expose air or oxygen to ionizing radiation.
Use ionized oxygen.
Ionize air to trap pollutants in an air cleaner.
Replace ozonized (or ionized) oxygen with ozone.
Speed up chemical reactions by ionizing the gas before use.
 

Principle 39. Inert atmosphere

Replace a normal environment with an inert one.
Prevent degradation of a hot metal filament by using an argon atmosphere.
Add neutral parts, or inert additives to an object.
Increase the volume of powdered detergent by adding inert ingredients. This makes it easier to measure with conventional tools.
 

Principle 40. Composite materials

Change from uniform to composite (multiple) materials.
Composite epoxy resin/carbon fiber golf club shafts are lighter, stronger, and more flexible than metal. Same for airplane parts.
Fiberglass surfboards are lighter and more controllable and easier to form into a variety of shapes than wooden ones.
